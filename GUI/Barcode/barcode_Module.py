import uuid

# format of code will be:
#   unique 10 digit id code. First 10 digits of upc. UPC stored in 
#   possible 9 digit name. SKU Code
#   - character seperator
def barcode_Gen(x_input=0):
    x = input("Enter a value: ") or  x_input
    print (x)


def barcode_Decode(x_input=0):
    pass

def unique_Id_Gen():
# upc will be completely unique per item.
    upc = uuid.uuid4()
    return upc

def SKU_Gen(Number, unique_Number):
# first digit will be a boolean for type 1 or 2

# type 2:
# > 1000 items
# next 5 will be number of items per product type => 99000 inventory
# last 3 will be unique product type number => 999 unique items


# type 2: 
# next 5 will be unique product type number => 99999 unique items
# the last 3 will be the stock number of the item => 999 items
    sku = ""

    if Number >= 1000 and Number < 99999 and unique_Number < 1000:
        sku = sku + "1" + str(Number).zfill(5) + str(unique_Number).zfill(3)
        print("Generated sku: " + sku)
        return(sku)
    elif Number <1000 and unique_Number <99999:
        sku = sku + "0" +str(unique_Number).zfill(5) + str(Number).zfill(3)
        print("Generated sku: " + sku)
        return(sku)
    else:
        print("Invalid input settings")
        
    
    

    
if __name__ == "__main__":


    barcode_Gen()

    SKU_Gen(1000,999)

    SKU_Gen(999,1000)
