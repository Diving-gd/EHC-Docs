import PySimpleGUI as sg
import requests

#Functions

def Add_item(Name, Description, StockNumber):
    response = requests.post('http://localhost:3000/Inventory', data ={'Name': Name, 'Description': Description, 'StockNumber': StockNumber})
    print(response)
	
def Add_user(Name):
    response = requests.post('http://localhost:3000/Inventory', data ={'Name': Name})
    print(response)

def Add_user_item(member, Itemid):
    response = request.post('http://localhost:3000/Inventory/member', data = {'Itemid': Itemid})
    print(response)

def Find_user(ID):
    response = requests.get('http://localhost:3000/Inventory', params = {'Name':ID})
    data = response.json()
    for i in range(len(data)):
        if int(data[i]['Name']) == int(ID):
           return(True)

#window definition
#def Main():

#Theme
rpi_theme = {'BACKGROUND': '#ffffff',
        'TEXT':'#D2042D',
        'INPUT':'#790604',
        'TEXT_INPUT': '#ffffff',
        'SCROLL': '#c7e78b',
        'BUTTON': ('#D2042D', '#FFFFFF'),
        'PROGRESS': ('#01826B', '#D0D0D0'),
        'BORDER': 1,
        'SLIDER_DEPTH': 0,
        'PROGRESS_DEPTH': 0
        }

sg.theme_add_new('rpitheme', rpi_theme )

sg.theme('rpitheme')

#tab configuration
Add_User_item_tab = [[sg.Text('Add Item to Member')],
                 [sg.Text('Member Name'),
                 sg.InputText(key='Member')],
                 [sg.Text('Item name'),
                 sg.InputText(key='Itemid')],
                 [sg.Button('Add Item',key='new_user_item')]]

Add_User_tab = [[sg.Text('Add Item')],
                 [sg.Text('Item name'),
                 sg.InputText(key='-IN-')],
                 [sg.Text('Description:'),
                 sg.InputText(key='-fName-')],
                 [sg.Text('Stock Number:'),
                  sg.InputText(key='-lName-')],
                 [sg.Button('Add Item',key='new_item')]]

layout = [[sg.Text('Welcome to EHC login')],
        [sg.TabGroup([
            [sg.Tab('Sign In',Add_User_item_tab)],
            [sg.Tab('Add Item', Add_User_tab)]]
            , key='-group1-', tab_location = 'top', selected_title_color='red')]]

window = sg.Window('Window Title',layout , alpha_channel = 1, grab_anywhere = True)  


#Persistance Loop
while True:
    event, values = window.read()

    if event == 'new_item': 
        Add_item(values['-IN-'],values['-fName-'], values['-lName-'])
        print(values['-IN-'],values['-fName-'], values['-lName-'], "added to database")

    if event == '-IN2-' and len(values['-IN2-']) > 3:
        # The if statement that triggers opening the door on correct id. 
        if Find_user(values['-IN2-']):
            print('User Found')
            window['-IN2-'].update('')

    if event == 'new_user_item':
        Add_user_item(values['member'],values['Itemid'])
        print(values['Itemid'],"added to member ",values['member'])

    if event == sg.WIN_CLOSED or event == 'Exit':           # always,  always give a way out!
        break

# Last Line Pls
window.close()
