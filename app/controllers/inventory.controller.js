const item = require('../models/inventory.model.js');

//Create and Save a new item
exports.create = (req,res) => {
	//Validate request
	if(!req.body.Description || !req.body.Name || !req.body.StockNumber){
		return res.status(400).send({
			message: "Item does not have enough identification"
		});
	}
	
	const Item = new item({
		Name: req.body.Name || "Un-named",
		Description: req.body.Description || "Non-descript",
		StockNumber: req.body.StockNumber || 1, 
		TimeOut: 0,
		TimeLeft: 150,
		Value: 50,
		RestockCode: "N/A",
		Stock: true
	});
	
	Item.save()
	.then(data => {
		res.send(data);
	}).catch(err => {
		res.status(500).send({
			message: err.message || "Some error occured while creating an Item"
		});
	});
};

// Retrieve and return all items from the database
exports.findAll = (req, res) => {
	item.find()
	.then(items => {
		res.send(items);
	}).catch(err => {
		res.status(500).send({
			message: err.message || "some error occured while retrieving Items"
		});
	});

};

// Find a single note with a noreId
exports.findOne = (req, res) => {
	item.findById(req.params.itemId)
	.then(Item => {
		if(!item){
			return res.status(404).send({
				message: "Item not found with id " + req.params.itemId
			});
		}
		res.send(Item);
	}).catch(err => {
		if(err.kind === 'ObjectId'){
			return res.status(404).send({
				message: "Item not found with id" + req.params.itemId
			});
		}
		return res.status(500).send({
			message:"Error retrieving item with id " + req.params.itemId
		});
	});

};

// Update an item identified by the itemId in the request
exports.update = (req, res) => {
	//Validate Request
	if(!req.body.Description){
		return res.status(400).send({
			message: "Item description cannot be empty"
		});
	}

	item.findByAndUpdate(req.patams.itemId, {
		Name: req.body.Name || "Un-named",
		Description: req.body.Description
	}, {new:true})
	.then(Item => {
		if(!Item) {
			return res.status(404).send({
				message: "Item not found with id " + req.params.itemId
			});
		}
		res.send(Item);
	}).catch(err => {
		if(err.kind === 'ObjectId') {
			return res.status(404).send({
				message: "Item not found with id " + req.params.itemId
			});
		}
		return res.status(500).send({
			message: "Error updating Item with id " + req.params.itemId
		});
	});	

};

// Delete a note with the specified itemId in the request
exports.delete = (req, res) => {
	item.findByIdAndRemove(req.params.noteId)
	.then(Item => {
		if(!Item){
			return res.status(404).send({
				message: "Item not found with id " + req.params.itemId
			});
		}
		return res.status(500).send({
			message: "Could not delete note with id " + req.params.itemId
		});
	});

};


