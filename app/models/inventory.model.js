const mongoose = require('mongoose');

const InventorySchema = mongoose.Schema({
    	Name:String,
    	Description: String,
	RestockCode: String,
	Value: Number,
	TimeOut: Number,
	TimeLeft: Number,
	StockNumber: Number,
	Stock: Boolean,
	
} ,{
    timestamps: true
});

module.exports = mongoose.model('Inventory', InventorySchema);
