module.exports = (app) => {
	const item = require('../controllers/inventory.controller.js');

	// Create a new Item
	app.post('/Inventory', item.create);

	// Retrieve all Items
	app.get('/Inventory', item.findAll);

	// Retrieve a single Item with itemId
	app.get('/Inventory/:itemId', item.findOne);

	// Update an Item with itemId
	app.put('/Inventory/:itemId', item.update);

	// Delete an Item with itemId
	app.delete('/Inventory/:itemId',item.delete);

}	
